//
//  ViewController.swift
//  TimerTask
//
//  Created by Venu on 8/1/20.
//  Copyright © 2020 Venu. All rights reserved.
//

import UIKit

final class ViewController: UIViewController {
    
    @IBOutlet weak var tableViewBox: UITableView!
    fileprivate var timerValues = [Int]() {
        didSet {
            tableViewBox.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        navigationController?.isNavigationBarHidden = false
        navigationItem.title = "TimerTask"
        registerTableViewCell()
        timerValues = [600, 1200, 1800, 3600]
    }
    
    private func registerTableViewCell() {
        tableViewBox.registerNib(TimerTableViewCell.self)
        tableViewBox.separatorStyle = .none
        tableViewBox.rowHeight = 44
        tableViewBox.estimatedRowHeight = UITableView.automaticDimension
    }
    
}

// MARK: Delegate And DataSource Methods
extension ViewController: UITableViewDelegate, UITableViewDataSource {
    
    // MARK: UITableview Delegate and Datasource
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return timerValues.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(forIndexPath: indexPath) as TimerTableViewCell
        cell.setContent(time: timerValues[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
}
