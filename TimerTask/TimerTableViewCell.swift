//
//  TimerTableViewCell.swift
//  TimerTask
//
//  Created by Venu on 8/1/20.
//  Copyright © 2020 Venu. All rights reserved.
//

import UIKit

final class TimerTableViewCell: UITableViewCell {
    
    @IBOutlet weak var viewMain: UIView!

    @IBOutlet weak var labelHeader: UILabel!
    @IBOutlet weak var labelTimer: UILabel!
    @IBOutlet weak var buttonStart: UIButton!
    @IBOutlet weak var buttonPause: UIButton!
    @IBOutlet weak var buttonReset: UIButton!
    
    var totalSeconds = 0
    var seconds = 0
    var timer = Timer()
    var isTimerRunning = false //This will be used to make sure only one timer is created at a time.
    var resumeTapped = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        selectionStyle = .none
        viewMain.layer.borderColor = UIColor.white.cgColor
        viewMain.layer.borderWidth = 1
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func setContent(time: Int) {
        print("Time:", Int(time) / 60, "\(Int(time) / 60)")
        labelHeader.text = "Timer: \(Int(time) / 60) Minutes"
        buttonPause.isEnabled = false
        totalSeconds = time
        seconds = time
    }
    
    @IBAction func startButtonTapped(_ sender: UIButton) {
        if isTimerRunning == false {
            runTimer()
            self.buttonStart.isEnabled = false
        }
    }
    
    @IBAction func pauseButtonTapped(_ sender: UIButton) {
        if self.resumeTapped == false {
            timer.invalidate()
            self.resumeTapped = true
            self.buttonPause.setTitle("Resume",for: .normal)
        } else {
            runTimer()
            self.resumeTapped = false
            self.buttonPause.setTitle("Pause",for: .normal)
            
        }
    }
    
    @IBAction func resetButtonTapped(_ sender: UIButton) {
        timer.invalidate()
        seconds = totalSeconds
        labelTimer.text = timeString(time: TimeInterval(seconds))
        isTimerRunning = false
        buttonStart.isEnabled = true
        buttonPause.isEnabled = false
    }
    
    func runTimer() {
        timer = Timer.scheduledTimer(timeInterval: 1, target: self,   selector: (#selector(updateTimer)), userInfo: nil, repeats: true)
        isTimerRunning = true
        buttonPause.isEnabled = true
        
    }
    
    @objc func updateTimer() {
        seconds -= 1     //This will decrement(count down)the seconds.
        labelTimer.text = timeString(time: TimeInterval(seconds))
    }
    
    func timeString(time:TimeInterval) -> String {
        let hours = Int(time) / 3600
        let minutes = Int(time) / 60 % 60
        let seconds = Int(time) % 60
        return "\(hours.twoDecimal) : \(minutes.twoDecimal) : \(seconds.twoDecimal)"
    }
    
    
}

extension Int {
    var twoDecimal: String {
        return String(format: "%02d", self)
    }
}
